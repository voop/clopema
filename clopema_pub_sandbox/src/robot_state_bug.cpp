/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: Oct 15, 2013
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *
 *  To run:
 *      roslaunch clopema_pub_moveit_config demo.launch
 *      rosrun clopema_pub_sandbox cb_robot_state
 */

#include <ros/ros.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>
#include <moveit/move_group_interface/move_group.h>

int main(int argc, char **argv) {
    ros::init(argc, argv, "rs_bug");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);
    spinner.start();

    moveit::planning_interface::MoveGroup g("arms");

    robot_state::RobotState rs(*g.getCurrentState());
    rs.updateLinkTransforms();
    rs.update(true);
    robot_model::RobotModel model(*rs.getRobotModel());

    const robot_model::LinkModel *lm = model.getLinkModel("r1_ee");
    if (!lm)
        return false;

    const robot_model::LinkTransformMap &fixed_links =
            lm->getAssociatedFixedTransforms();

    /*
      rosrun tf tf_echo r1_ee r1_tip_link
    - Translation: [0.000, 0.000, -0.348]
    - Rotation: in Quaternion [0.000, 0.000, 1.000, -0.000]
                in RPY [0.000, -0.000, -3.142]
    */

    /* Result without fix:
        [ INFO] [1384156201.690553112]: r1_tip_link
        [ INFO] [1384156201.690635455]:
        -1 2.65359e-06           0           0
        -2.65359e-06          -1           0           0
          0           0           1    -0.24775
          0           0           0           1

    */
    /* Result after fix:
     *
        [ INFO] [1384156694.860179826]: r1_tip_link
        [ INFO] [1384156694.860326796]:          -1 2.65359e-06           0           0
        -2.65359e-06          -1           0           0
          0           0           1    -0.34775
          0           0           0           1
    */
    for (robot_model::LinkTransformMap::const_iterator it = fixed_links.begin();
            it != fixed_links.end(); ++it) {
        ROS_INFO_STREAM(it->first->getName());
        ROS_INFO_STREAM(it->second.matrix());
    }

    spinner.stop();
    return 0;
}
