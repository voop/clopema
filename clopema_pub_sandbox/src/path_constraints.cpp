#include <ros/ros.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/planning_scene_monitor/current_state_monitor.h>
#include <moveit/move_group_interface/move_group.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit/planning_pipeline/planning_pipeline.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit/robot_state/conversions.h>

int main(int argc, char **argv) {
    ros::init(argc, argv, "path_constraints_tut");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool use_path_const = true; //use path position constraints
    node.param("path", use_path_const, true);
    ROS_INFO_STREAM("Use path constraints: " << use_path_const);

    move_group_interface::MoveGroup g("r1_arm");
    g.setPlannerId("RRTkConfigDefault");

    moveit_msgs::PositionConstraint pc;
    { //Set position constraint
        moveit_msgs::BoundingVolume reg;
        shape_msgs::SolidPrimitive prim;
        prim.type = prim.BOX;
        prim.dimensions.resize(3);
        prim.dimensions[0] = 5;
        prim.dimensions[1] = 2.0001;
        prim.dimensions[2] = 5;

        geometry_msgs::Pose pose;
        pose.position.x = 0;
        pose.orientation.w = 1;

        reg.primitives.push_back(prim);
        reg.primitive_poses.push_back(pose);
        pc.header.stamp = ros::Time::now();
        pc.constraint_region = reg;
        pc.link_name = "r1_tip_link";
        pc.header.frame_id = "base_link";
        pc.weight = 1.0;
    }
    moveit_msgs::Constraints c;
    c.name = "testas";
    c.position_constraints.push_back(pc);

    g.setStartStateToCurrentState();
    geometry_msgs::Pose pose;
    pose.orientation.y = 1;
    pose.position.x = -0.2;
    pose.position.y = -1.0;
    pose.position.z = 1.2;
    g.setJointValueTarget(pose, "r1_tip_link");
    g.move();
    g.setStartStateToCurrentState();
    if (use_path_const)
        g.setPathConstraints(c);
    pose.position.x = -1.0;
    g.setJointValueTarget(pose, "r1_tip_link");
    g.move();

    spinner.stop();
    ros::shutdown();
    return 0;
}
