#include <ros/ros.h>
#include <moveit/move_group_interface/move_group.h>
#include <moveit/robot_state/conversions.h>
#include <eigen_conversions/eigen_msg.h>

bool use_move_cmd = false;

robot_state::AttachedBody* attached_body(const robot_state::RobotState& rs,
                                         const Eigen::Affine3d& t);

bool moveArm(const Eigen::Affine3d& epose, const std::string armName,
             const float speed = 0.1) {
    geometry_msgs::Pose pose;
    tf::poseEigenToMsg(epose, pose);

    moveit::planning_interface::MoveGroup crc(armName + "_arm");
    //crc.setRobotSpeed(speed);

    robot_state::RobotState rs(*crc.getCurrentState());
    rs.attachBody(attached_body(rs, epose));
    crc.setStartState(rs);
    crc.setJointValueTarget(pose, armName + "_ee");

    //You cannot use move() command here because start state will be ignored in that case.
    bool success = false;
    if(use_move_cmd) {
        success = crc.move();
    } else {
        moveit::planning_interface::MoveGroup::Plan plan;
        success = crc.plan(plan);
        if (success)
            success = crc.execute(plan);
    }
    return success;
}

int main(int argc, char *argv[]) {
    ros::init(argc, argv, "att_body_tut");
    ros::NodeHandle n("~");

    n.param("use_move", use_move_cmd, false);
    ros::AsyncSpinner spinner(1);
    spinner.start();

    moveit::planning_interface::MoveGroup crc("arms");
    crc.setNamedTarget("arms_home");
    crc.move();

    Eigen::Affine3d pose = Eigen::Affine3d::Identity();
    pose.rotate(Eigen::Quaterniond(0, 1, 0, 0));
    pose.translation() = Eigen::Vector3d(0.2, -0.8, 1.4);

    moveArm(pose, "r2");

    pose = Eigen::Affine3d::Identity();
    pose.rotate(Eigen::Quaterniond(0.5, 0.5, 0.5, 0.5));
    pose.translation() = Eigen::Vector3d(0.2, -0.8, 1.2);
    moveArm(pose, "r1");

    return 0;
}

robot_state::AttachedBody* attached_body(const robot_state::RobotState& rs,
                                         const Eigen::Affine3d& t) {
    boost::shared_ptr<shapes::Box> shape;
    shape.reset(new shapes::Box(0.2, 0.2, 0.5));
    Eigen::Affine3d f = Eigen::Affine3d::Identity();
    f.translation() = Eigen::Vector3d(0, 0, 0.3);
    std::vector<std::string> touch_lnks;
    /*touch_lnks.push_back("r1_tip_link");
     touch_lnks.push_back("r1_gripper");
     touch_lnks.push_back("r1_link_6");
     touch_lnks.push_back("r1_link_7");*/

    std::vector<shapes::ShapeConstPtr> shapes;
    shapes.push_back(shape);
    EigenSTL::vector_Affine3d attach_trans;
    attach_trans.push_back(f);
    std::set<std::string> touch_links(touch_lnks.begin(), touch_lnks.end());

    robot_state::AttachedBody *sensor_body;
    sensor_body = new robot_state::AttachedBody(rs.getLinkModel("r2_ee"), "box",
                                                shapes, attach_trans, touch_links,
                                                trajectory_msgs::JointTrajectory());
    return sensor_body;
}
